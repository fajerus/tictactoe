#include "gamer.h"
#include <iostream>

using namespace std;

Gamer::Gamer(){
 // ctor
}


void Gamer::move(Board &board){
    int move = -2;
    do {
        cout << "\nInput move ([0..8]): ";
        cin  >> move;
        if(rightMove(board.grid, move)){
            board.grid[move] = -1;
            break;
        }else{
            cout << "incorrect move =| ";
        }
        cout << endl;
    } while (  true  );
}

bool Gamer::rightMove(int grid[9], int move){
    if(move >= 0 && move <= 8){
        if(grid[move] != 0)
            return false;
        return true;
    }else{
        return false;
    }
}

#ifndef BOARD_H
#define BOARD_H

class Board{
    public:
        int grid[9];

        Board();

        char gridChar(int pos);                 // обозначение символа игрока на игровом поле
        int  win(  );                             // возврат номера игрока, победившего в игре

        void printGrid();                       // распечатка игрового поля

    protected:
    private:
       const int wins[8][3] = {{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{2,4,6}};

};

#endif // BOARD_H

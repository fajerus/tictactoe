#ifndef TICTAC_H
#define TICTAC_H

#include "gamer.h"
#include "computerai.h"

class TicTac {
    public:
        TicTac();                               // инициализация системных переменных
        virtual ~TicTac();                      // не нужен, но пусть будет =)

        void letsBeginPlay();                   // игра

        int wins;
        int draws;
        int loses;

    protected:
    private:
        int player = 0;
    // methods

        void printTitle();
        // players
        Gamer gamer;
        ComputerAI computer;

};

#endif // TICTAC_H

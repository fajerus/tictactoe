#include <iostream>
#include <stdlib.h>

#include "tictac.h"


using namespace std;

int main(){

    TicTac game;

    bool gameloop = true;
    char cont;
    while(gameloop){
        system("cls");
        game.letsBeginPlay();
        cout << "\n\n Do you want to restart game? (y - yes, n - no): \n";
        do{
            cin >> cont;
            if( cont != 'y' && cont != 'n' ){
                cout << "Please put  the correct data: ";
            }else{
                if( cont == 'n' ){
                    gameloop = false;
                    cout << "Total:\nwins: " << game.wins << "\ndraws: " << game.draws << "\nloses: " << game.loses << endl;;
                } else if( cont == 'y' ){
                    gameloop = true;
                }else{
                    // сюда мы не попадем
                }

            }
        }while( cont != 'y' && cont != 'n' );

    }

//
    return 0;
}

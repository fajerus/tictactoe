#include "board.h"
#include <iostream>

using namespace std;

Board::Board(){
   for(int i=0; i<9; i++){
    grid[i] = 0;
  }
}




void Board::printGrid( ){

    cout << "|-----------|" << endl;
    for(int i=0; i<9; i+=3) {
        cout << "| " <<  gridChar(grid[i]) << " | " << gridChar(grid[i+1]) << " | " << gridChar(grid[i+2]) << " |" << endl;
        if(i % 3 == 0 && i != 6)
            cout << "|-----------|" << endl;
    }
    cout << "|-----------|" << endl;

}

int  Board::win( ){
    for(int i = 0; i < 8; ++i) {
        if(grid[wins[i][0]] != 0 &&
                grid[ wins[i][0] ] == grid[ wins[i][1] ] &&
                grid[ wins[i][0] ] == grid[ wins[i][2] ])
            return grid[ wins[i][2] ];
    }
    return 0;
}

char Board::gridChar(int pos){
    if ( pos == -1 ){
        return 'X';
    } else if ( pos == 0 ) {
        return ' ';
    } else if ( pos == 1 ) {
        return 'O';
    }
}

#include <iostream>
#include "tictac.h"
#include "board.h"

using namespace std;

TicTac::TicTac(){
    wins = draws = loses = 0;
}

TicTac::~TicTac()
{
    //dtor
}



void TicTac::letsBeginPlay(){

    Board board;

    printTitle();
    cout << "\n";
    cout << "Notations: \n You - X; \nComputer - 0; \n\n\n";
    cout << "Game statistic: \nwins: " << wins << "\ndraws: " << draws << "\nloses: " << loses << "\n\n";
    cout << "Choise, who will start the game: 1 - you, 2 - computer \n";

    do{
        cin >> player;
        if(player > 2 || player < 1){
            cout << "Incorrect number, enter the valid number: \n";
        }
    }while(player > 2 || player < 1);



    for(int step=0; step < 9 && board.win(  ) == 0; ++step){
        if( ( step + player ) % 2 == 0 ){
            computer.move(board);
        } else {
            board.printGrid();
            gamer.move(board);
        }
    }

    switch(board.win(  )) {
        case 0:
            board.printGrid();
            draws++;
            cout << "A draw =/  \n";
            break;
        case 1:
            loses++;
            board.printGrid();
            cout << "You lose! ;( Don't worry! .\n";
            break;
        case -1:
            wins++;
            board.printGrid();
            cout << "You win!!! Unbeliaveable! =) \n";
            break;
    }


}

void TicTac::printTitle(){

    cout << "   ---------------------------------------------------------------------------"           << endl;
    cout << "     _____ _    _____        _____            _____                        _ "            << endl;
    cout << "    |_   _(_)  |_   _|      |_   _|          |  __ \\                      | |"           << endl;
    cout << "      | |  _  ___| | __ _  ___| | ___   ___  | |  \\/ __ _ _ __ ___   ___  | |"           << endl;
    cout << "      | | | |/ __| |/ _` |/ __| |/ _ \\ / _ \\ | | __ / _` | '_ ` _ \\ / _ \\ | |"        << endl;
    cout << "      | | | | (__| | (_| | (__| | (_) |  __/ | |_\\ \\ (_| | | | | | |  __/ |_|"          << endl;
    cout << "      \\_/ |_|\\___\\_/\\__,_|\\___\\_/\\___/ \\___|  \\____/\\__,_|_| |_| |_|\\___| (_)" << endl;
    cout << "   ---------------------------------------------------------------------------"           << endl;

}

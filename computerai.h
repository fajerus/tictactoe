#include "board.h"

#ifndef COMPUTERAI_H
#define COMPUTERAI_H


class ComputerAI{
    public:
        ComputerAI();
        void move( Board &board );
    protected:
    private:
        int minimax(Board board, int player);

};

#endif // COMPUTERAI_H

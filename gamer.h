#ifndef GAMER_H
#define GAMER_H

#include "board.h"

class Gamer{
    public:
        Gamer();
        void move( Board &board );
    protected:
    private:

        bool rightMove(int grid[9], int move);
};

#endif // GAMER_H

#include "computerai.h"
#include "board.h"

ComputerAI::ComputerAI(){
    //ctor
}


int  ComputerAI::minimax(Board board, int player){

    int winner = board.win(  );
    if(winner != 0) return winner * player;

    int move = -1;
    int score = -2;//Losing moves are preferred to no move
    int i;
    for(i = 0; i < 9; ++i) {//For all moves,
        if( board.grid[i] == 0 ) {//If legal,
            board.grid[i] = player;//Try the move
            int thisScore = -1 * minimax( board ,  player * -1);
            if(thisScore > score) {
                score = thisScore;
                move = i;
            }//Pick the one that's worst for the opponent
            board.grid[i] = 0;//Reset board after try
        }
    }
    if(move == -1)
        return 0;
    return score;
}

void ComputerAI::move(Board &board){
    int move = -1;
    int score = -2;
    for(int i = 0; i < 9; ++i) {
        if(board.grid[i] == 0) {
            board.grid[i] = 1;
            int tempScore = -minimax( board ,  -1 );
            board.grid[i] = 0;
            if(tempScore > score) {
                score = tempScore;
                move = i;
            }
        }
    }
//returns a score based on minimax tree at a given node.
    board.grid[move] = 1;
}
